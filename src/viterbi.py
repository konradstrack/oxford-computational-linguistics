__author__ = 'Konrad Strack'

import logging
from operator import itemgetter

logger = logging.getLogger(__name__)


class Array2d():
    """
    A utility class to represent a 2D array.
    (Implementation inspired by http://stackoverflow.com/a/9490755).
    """

    def __init__(self, type, height, width):
        """
        Initialises a height by width array with zeroes.
        :param type: type of the array
        :param height: height of the array
        :param width: width of the array
        """
        self.height = height
        self.width = width
        self.data = [[0 for _ in range(width)] for _ in range(height)]

    def __getitem__(self, index):
        return self.data[index[0]][index[1]]

    def __setitem__(self, index, value):
        self.data[index[0]][index[1]] = value


class Viterbi:
    """
    An implementation of the Viterbi algorithm.
    """

    def __init__(self, tags, tag_prob, word_prob):
        self.__tag_prob = tag_prob
        self.__word_prob = word_prob
        self.__tags = tags

    def tag_text(self, text):
        """
        Tags untokenized text.

        :param text: text to tag
        :return: list of tags
        """
        tokens = self.tokenize_text(text)
        return self.tag_tokenized_text(tokens)

    def tag_tokenized_text(self, tokens):
        """
        Tags a text / sentence.

        :param tokens: list of tokens
        :return: list of tags
        """
        n = len(tokens)  # number of words
        k = len(self.__tags)  # number of tags

        # initialize scores
        scores = [[0 for _ in range(n)] for _ in range(k)]
        backpointers = [[0 for _ in range(n)] for _ in range(k)]

        for i, tag in enumerate(self.__tags):
            scores[i][0] = self.__word_prob.get(tokens[0], tag) + self.__tag_prob.get(tag, 'START')
            logger.debug("Score initialization: {0}, {1}, {2}".format(i, tag, scores[1][0]))

        for j, word in enumerate(tokens[1:]):
            for i, tag in enumerate(self.__tags):
                previous_scores = [
                    (kk, scores[kk][j] + self.__word_prob.get(word, tag) + self.__tag_prob.get(tag, kt))
                    for kk, kt in enumerate(self.__tags)]

                backpointer, score = max(previous_scores, key=itemgetter(1))
                scores[i][j + 1] = score
                backpointers[i][j + 1] = backpointer

        return list(zip(tokens, self.retrieve_tags(n, k, scores, backpointers)))

    def retrieve_tags(self, n, k, scores, backpointers):
        """
        Retrieves tags for a text based on scores and backpointers tables from Viterbi algorithm.

        :param n: number of words
        :param k: number of tags
        :param scores: k x n table with scores
        :param backpointers: k x n table with backpointers
        :return: list of tags
        """
        ti, _ = max([(i, scores[i][n - 1]) for i in range(k)], key=itemgetter(1))
        logger.debug("Tag scores: {0}".format([(i, scores[i][n - 1]) for i in range(k)]))

        tags = [self.__tags[ti]]
        for i in reversed(range(n - 1)):
            ti = backpointers[ti][i + 1]
            tags.append(self.__tags[ti])

        return reversed(tags)

    @staticmethod
    def tokenize_text(text):
        """
        Splits given text into tokens.

        :param text: text to split
        :return: list of tokens
        """
        return text.split()