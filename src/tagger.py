from itertools import chain
from math import ceil
import re

__author__ = 'Konrad Strack'

from argparse import ArgumentParser
import logging

from probability import get_tag_probabilities, get_word_probabilities
from tags import extend_tags_with_np, tags_from_file, correct_tags
from viterbi import Viterbi
from pos_extract import all_texts_from


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def tag_sentence(sentence, texts, tags):
    """
    Tags words in a single sentence / text.

    :param sentence: sentence to tag
    :param texts: text on which the tagger should be train
    :param tags: set of tags used in the training data
    :return: list of tuples representing the tagged sentence
    """
    corrected_texts, exception_list = correct_tags(texts, tags)
    print("Exceptions: ", exception_list)

    tag_prob = get_tag_probabilities(corrected_texts)
    word_prob = get_word_probabilities(corrected_texts)

    viterbi = Viterbi(tags, tag_prob, word_prob)

    tagged = viterbi.tag_text(sentence)
    print(tagged)


def chunks(lst, n):
    """
    Creates a generator that splits a list into n chunks.

    :param lst: a list to split
    :param n: number of chunks
    """

    chunk_size = ceil(len(lst) / n)
    for i in range(0, len(lst), chunk_size):
        yield lst[i:i + chunk_size]


def measure_correctness(text, tagged_text):
    """
    Calculates ratio of correctly tagged words.

    :param text: original text
    :param tagged_text: tagged text
    :return: ratio of correctly tagged words, number of correctly tagged words, and total number of words
    """

    common = [i for i, j in zip(text, tagged_text) if tuple(i) == tuple(j)]
    word_count = len(text)
    correct_count = len(common)

    return correct_count / word_count, correct_count, word_count


def measure_np_correctness(text, tagged_text):
    """
    Calculates ratio of correctly tagged words, without taking into account non-NP words.

    :param text: original text
    :param tagged_text: tagged text
    :return: ratio of correctly tagged words, number of correctly tagged words, and total number of words
    """
    new_text = []
    for pair in text:
        if len(pair) > 1 and pair[1].endswith('-NP'):
            new_text.append(pair)
        else:
            new_text.append(())

    new_tagged_text = []
    for pair in tagged_text:
        if len(pair) > 1 and pair[1].endswith('-NP'):
            new_tagged_text.append(pair)
        else:
            new_tagged_text.append(())

    correct_count = len([i for i, j in zip(new_text, new_tagged_text) if tuple(i) == tuple(j) and i is not ()])
    word_count = len([i for i in new_text if i is not ()])

    return correct_count / word_count, correct_count, word_count


def evaluate(train_data, test_data, tags, measure_only_np=False):
    """
    Performs one pass on training and test data.

    :param train_data: sentences to use for training
    :param test_data: sentences to use for testing
    :param tags: list of accepted tags
    :param measure_only_np: if true, will measure only NP-tagged words
    :return: result of the measurement
    """
    split_regex = re.compile(r'(?<!\\)/|')

    # train the model
    corrected_texts, _ = correct_tags(list(train_data), tags)
    tag_prob = get_tag_probabilities(corrected_texts)
    word_prob = get_word_probabilities(corrected_texts)

    viterbi = Viterbi(tags, tag_prob, word_prob)

    # tag all test data at once
    text = chain(*test_data)

    wt_pairs = [split_regex.split(w, maxsplit=1) for w in text]
    tokenized_text = [p[0] for p in wt_pairs]
    tagged = viterbi.tag_tokenized_text(tokenized_text)

    measure = measure_np_correctness if measure_only_np else measure_correctness
    return measure(wt_pairs, tagged)


def cross_validation(texts, tags, k, train_on_all=False, measure_only_np=False):
    """
    Performs a k-fold cross-validation of the tagger.

    :param texts: collection of texts
    :param tags: available tags
    :param k: 1/k of texts will be used as test data
    :param train_on_all: if True, will use all data for training (including 1/k used as test data)
    :return: accuracy of each pass
    """

    data_chunks = list(chunks(texts, k))
    results = []

    for i in range(k):
        test_data = data_chunks[i]

        if train_on_all:
            train_data = texts
        else:
            # train_data = chain(chain(*data_chunks[:i]), chain(*data_chunks[i + 1:]))
            train_data = [text for chunk in data_chunks[:1] for text in chunk]
            train_data.extend([text for chunk in data_chunks[i + 1:] for text in chunk])

        correctness, correct_count, word_count = evaluate(train_data, test_data, tags, measure_only_np)
        results.append(correctness)
        logger.info("Evaluation result (pass {3}): {0}/{1}, {2}".format(correct_count, word_count, correctness, i))

    return results


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("-p", "--pos-location", type=str, required=True, dest="pos_files_path",
                        help="directory in which the training data is located (.POS files)")
    parser.add_argument("-t", "--tags-file", type=str, required=True, dest="tags_file_path",
                        help="file containing a newline-separated list of accepted tags")
    parser.add_argument("--np-tags-file", type=str, dest="np_tags_file_path",
                        help="file containing a newline-separated list of tags used in noun phrases")
    parser.add_argument("-l", "--limit", type=int, dest="limit", help="limit the number of texts")
    parser.add_argument("-s", "--sentence", dest="sentence", help="sentence to tag")

    parser.add_argument("-c", "--cross-validate", dest="cross_validate", action="store_true")
    parser.add_argument("-k", dest="cross_validation_chunks", type=int, default=10,
                        help="number of chunks for cross validation")
    parser.add_argument("-a", dest="train_on_all", action="store_true",
                        help="use all data as training data for cross-validation")
    parser.add_argument("-n", "--np", dest="use_np_chunking", action="store_true",
                        help="extract noun phrases")
    parser.add_argument("-o", "--only-np", dest="measure_only_np", action="store_true",
                        help="measure only correctness of NP tags")

    args = parser.parse_args()

    texts = all_texts_from(args.pos_files_path, args.use_np_chunking)

    # limit the number of texts
    if args.limit is not None:
        texts = texts[:args.limit]

    tags = tags_from_file(args.tags_file_path)
    if args.use_np_chunking:
        np_tags = tags_from_file(args.np_tags_file_path)
        tags = extend_tags_with_np(tags, np_tags)

    if args.sentence is not None:
        tag_sentence(args.sentence, texts, tags)

    if args.cross_validate:
        cross_validation_chunks = args.cross_validation_chunks
        results = cross_validation(texts, tags, cross_validation_chunks, args.train_on_all, args.measure_only_np)

        average = sum(results) / len(results)
        stddev = (sum([(x - average) ** 2 for x in results]) / len(results)) ** 0.5
        print("Correctness: {0} with standard deviation {1}".format(average, stddev))
