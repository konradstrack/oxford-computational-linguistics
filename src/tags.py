from copy import copy
import re

__author__ = 'Konrad Strack'


def fix_tags(text_tokens, tags):
    """
    Fixes tags (handles exceptions) in a tokenized text based on a list of accepted tags.
    :param text_tokens: list of tokens in a text
    :param tags: list of accepted tags
    :return: list of fixed (word, tag) pairs and an analogical list of exceptions
    """
    # exceptions are all (word, tag) pairs where the tag doesn't match any of the tags from the provided list
    exceptions = []

    # list of correct and corrected (word, tag) pairs
    correct = []

    # turn tags into a set so that searching is faster
    stags = set(tags)

    split_regex = re.compile(r'(?<!\\)/|')

    for token in text_tokens:
        try:
            word, tag = split_regex.split(token, maxsplit=1)
        except ValueError:
            word, tag = token.split(r'/', maxsplit=1)

        if tag not in stags:
            exceptions.append((word, tag))
            correct.extend(select_tag(word, tag, stags))
        else:
            correct.append((word, tag))

    return correct, exceptions


def select_tag(word, tag, accepted_tags):
    """
    Selects a tag for a word in case a tag is unusual.

    :param word: a word
    :param tag: a tag to analyse
    :param accepted_tags: list of available tags
    :return: a list with (word, tag) pairs or an empty list if a suitable tag cannot be found
    """
    for t in re.split(r"\W+", tag):
        if t in accepted_tags:
            return [(word, t)]

    return []


def extend_tags_with_np(tags, np_tags):
    """
    Extends a list of tags with enriched NP tags.

    :param tags: list of original tags
    :param np_tags: list of tags that can be used in noun phrases
    :return: a new list of tags
    """
    extended_tags = copy(tags)
    extended_tags.extend([tag + '-B-NP' for tag in np_tags])
    extended_tags.extend([tag + '-I-NP' for tag in np_tags])

    return extended_tags


def tags_from_file(path):
    """
    Reads tags from a file. Assumes that each tag is in a separate line.

    :param path: path to the file with tags
    :return: list of tags
    """

    with open(path, 'r') as f:
        return [line.strip() for line in f.readlines()]


def correct_tags(texts, tags):
    """
    Corrects tags in texts and retrieves a list of exceptions.

    :param texts: training texts
    :param tags: list of available tags
    :return: corrected texts, list of exceptions
    """
    corrected_texts = []
    exception_list = []

    for text in texts:
        correct, exceptions = fix_tags(text, tags)
        corrected_texts.append(correct)
        exception_list.extend(exceptions)

    return corrected_texts, exception_list