from collections import defaultdict
from math import log

__author__ = 'Konrad Strack'


class Probabilities:
    """
    A simple wrapper over a probability distribution of tags.
    """

    def __init__(self, probability_map, default_probability):
        self.probabilities = probability_map
        self.default_probability = default_probability

    def get(self, tag, given_tag):
        if (tag, given_tag) not in self.probabilities:
            return self.default_probability
        return self.probabilities[(tag, given_tag)]


class TagProbBuilder:
    """
    Builds a dictionary with logarithms of probabilities P(Cat_i | Cat_{i-1}).
    """

    __counts = defaultdict(float)

    def incr(self, tag, given_tag, by=1):
        self.__counts[(tag, given_tag)] += by

    def build(self, number_of_tags):
        total = sum(self.__counts.values())

        probability_map = {}
        for k, v in self.__counts.items():
            probability_map[k] = log((v + 1) / (total + number_of_tags))

        default_probability = log(1 / (total + number_of_tags))
        return Probabilities(probability_map, default_probability)


class WordProbBuilder:
    """
    Builds a dictionary of logarithms of probabilities P(Word | Cat).
    """

    __counts = defaultdict(lambda: defaultdict(float))

    def incr(self, word, given_tag, by=1):
        self.__counts[given_tag][word] += by

    def build(self, vocabulary_size):
        probability_map = {}

        for tag in self.__counts.keys():
            total = sum(self.__counts[tag].values())

            for word, v in self.__counts[tag].items():
                probability_map[(word, tag)] = log((v + 0.01) / (total + vocabulary_size))

        default_probability = log(0.01 / (total + vocabulary_size))
        return Probabilities(probability_map, default_probability)


def get_vocabulary_size(texts):
    """
    Calculates the number of unique words in a list of texts.

    :param texts: list of texts
    :return: number of unique words
    """
    return len({word for word, tag in [pair for text in texts for pair in text]})


def get_tag_count(texts):
    """Calculates the number of unique tags in a list of texts.

    :param texts: list of texts
    :return: number of unique tags
    """
    return len({tag for word, tag in [pair for text in texts for pair in text]})


def get_tag_probabilities(texts):
    """
    Builds probability distribution P(tag1|tag2).

    :param texts: training texts
    :return: object representing the probabilities
    """

    tag_prob_builder = TagProbBuilder()

    for text in texts:
        for p, q in zip([('**start**', 'START')] + text, text):
            tag_prob_builder.incr(q[1], p[1])

    count = get_tag_count(texts)
    return tag_prob_builder.build(count)


def get_word_probabilities(texts):
    """
    Builds probability distribution P(word|tag).

    :param texts: training texts
    :return: object representing the probabilities
    """

    word_prob_builder = WordProbBuilder()

    for text in texts:
        for p in text:
            word_prob_builder.incr(p[0], p[1])

    size = get_vocabulary_size(texts)
    return word_prob_builder.build(size)