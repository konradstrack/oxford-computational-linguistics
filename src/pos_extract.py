import os
import re

__author__ = 'Konrad Strack'


def all_texts_from(path, use_np_chunking):
    """
    Returns all tagged texts from .POS files in a given directory.
    :param path: a path to the directory
    :return: list of texts
    """
    tokenize = tokenize_pos_with_np if use_np_chunking else tokenize_pos

    texts = []

    pos_files = get_pos_files_in(path)
    for file_path in pos_files:
        with open(file_path, 'r') as file:
            content = file.read()
            texts.extend(tokenize(content))

    return texts


def get_pos_files_in(path):
    """
    Returns a list of all paths to .POS files in a given directory.
    :param path: a path to the directory
    :return: list of .POS files
    """

    pos_filename = re.compile(r'.+\.POS')

    # get all .POS files
    pos_files = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if pos_filename.match(file):
                pos_files.append(os.path.join(root, file))

    return pos_files


def tokenize_pos_with_np(content):
    """
    Tokenizes POS files. Takes into account noun phrases and enriches their tags with -B-NP and -I-NP.

    :param content: content of a file
    :return: list of tokenized sentences
    """
    chunks = re.split(r'\s*={5,}\s*', content)

    texts = []

    for text in chunks:
        sentences = re.split(r'(?!\])[\n]{2,}(?!\[)', text)
        for sentence in sentences:
            tokens = []
            for line in sentence.splitlines():
                line_tokens = tokenize_text(line)
                if line.startswith("["):
                    if len(line_tokens) > 0:
                        line_tokens[0] += '-B-NP'
                    for i in range(1, len(line_tokens)):
                        line_tokens[i] += '-I-NP'

                tokens.extend(line_tokens)

            texts.append(tokens)

    return texts


def tokenize_pos(content):
    """
    Tokenizes POS files into sentences and words.

    :param content: content of a file
    :return: list of tokenized sentences
    """
    # split into texts (i.e. chunks separated by ===+)
    chunks = re.split(r'\s*={5,}\s*', content)

    texts = []

    for text in chunks:
        sentences = re.split(r'(?!\])[\n]{2,}(?!\[)', text)
        for sentence in sentences:
            tokens = []
            for line in sentence.splitlines():
                tokens.extend(tokenize_text(line))

            texts.append(tokens)

    return texts


def tokenize_text(text):
    """
    Tokenizes a given text into (word, tag) pairs.

    :param text: text to tokenize
    :return: list of (word, tag) tuples
    """
    # find al word/tag pairs
    tokens = re.findall(r'\s*(\S+/\S+)', text)
    return tokens